'''
Created on 09-Sep-2020

@author: tom.bsc
'''
from Norway.PrivateSSN import PrivateSSN
from Norway.CorporateSSN import CorporateSSN


if __name__ == '__main__':
    print(f"Private Norway SSN number: {PrivateSSN().generate_private_ssn()}")
    print(f"Corporate Norway SSN number: {CorporateSSN().generate_corporate_ssn()}")