from random import randint
from robot.api.deco import keyword
 

class CorporateSSN():
    '''
    Class used to handle Norwegian corporate SSN number
    generation based on:
    https://www.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-identification-numbers/Norway-TIN.pdf
    '''

    _base_mod11_weights = [3, 2, 7, 6, 5, 4, 3, 2]

    def _get_eight_random_numbers(self):
        digits = ""
        digits += str(randint(8, 9)) #First digit has to be 8 or 9

        #Generating next 7 digits, last 
        for digit in range(7):
            digits += str(randint(1, 9))

        return digits

    def _calculate_checsum(self, stringNumber):

        # checking if both variables have the same length - they have to be
        # equal
        if len(stringNumber) != len(self._base_mod11_weights):
            raise Exception("Length of: {0} and {1} are not equal".format(
                stringNumber, self._base_mod11_weights))

        _product_sum = 0
        for idx, char in enumerate(stringNumber):
            _product_sum += self._base_mod11_weights[idx] * int(char)

        _checksum = _product_sum % 11
        if _checksum == 0 or _checksum == 1:
            raise Exception("Checksum for {0} equals: {1}".format(stringNumber, _checksum))
        return str(11 - _checksum)

    @keyword('Get Norwegian corporate SSN')
    def generate_corporate_ssn(self):
        """
        Generates Norwegian corporate/enterprise number
        
        :return  norwegian corporate number
        """
        _result = []
        for i in range(100):
            try:
                ssn=""
                ssn = self._get_eight_random_numbers()
                ssn += self._calculate_checsum(ssn)
                _result.append(ssn)
            except:
                continue
        return _result[randint(0, len(_result)-1)]
