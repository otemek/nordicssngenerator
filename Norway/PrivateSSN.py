'''
Created on 17 Dec 2019
Class responsible for generating Norwegian SSN Number
@author: maestro
'''
import datetime
from dateutil.relativedelta import relativedelta
from random import randint
from robot.api.deco import keyword

class PrivateSSN():
    _k1_weights = [2, 5, 4, 9, 8, 1, 6, 7, 3]
    _k2_weights = [2, 3, 4, 5, 6, 7, 2, 3, 4, 5]
    
    
    def _get_year_month_day(self):
        _year_month_day = datetime.date.today() - relativedelta(years=30)
        return int(_year_month_day.strftime("%d%m%y"))


    def _calculate_checksum(self, weights, numbers):
        _checksum = 0
        for i in range(len(numbers)):
            _product = int(weights[i]) * int(numbers[len(numbers) - 1 - i])
            _checksum += int(_product)
        return _checksum
    
    
    def _calculate_Mod11_Checksum(self, weights, stringNumber):
        _c = self._calculate_checksum(weights, stringNumber) % 11
        if _c == 1:
            raise Exception("invalid checksum for: {0}".format(stringNumber))
        return _c if _c == 0 else 11 - _c
    
    
    def _calculate_first_checksum_digit(self, stringNumber):
        return self._calculate_Mod11_Checksum(self._k1_weights, stringNumber)
    
    
    def _calculate_second_checksum_digit(self, stringNumber):
        return self._calculate_Mod11_Checksum(self._k2_weights, stringNumber)
    
    
    @keyword('Get Norwegian private SSN')
    def generate_private_ssn(self):
        """
        Keyword used to generate Norwegian SSN number:
        @return: random element from the list(generated ssn numbers)
        """
        
        _result = []
        for i in range(999, 900, -1):
            _fodselsnummer = ""
            _fodselsnummer += str(self._get_year_month_day())
            _fodselsnummer += str(i)
            try:
                _fodselsnummer += str(self._calculate_first_checksum_digit(_fodselsnummer))
                _fodselsnummer += str(self._calculate_second_checksum_digit(_fodselsnummer))
                _result.append(_fodselsnummer)
            except:
                continue
        return _result[randint(0, len(_result)-1)]
    