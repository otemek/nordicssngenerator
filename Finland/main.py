'''
Created on 09-Sep-2020

@author: maestro
'''

from Finland.PrivateSSN import get_finnish_ssn

if __name__ == '__main__':
    print(f"Finnish private ssn number: {get_finnish_ssn()}")