'''
Created on 26.2.2020

@author: maestro
'''

import datetime
from dateutil.relativedelta import relativedelta
import random
from robot.api.deco import keyword

""" _CENTRUY_SIGN
        The sign for the century is either:
        + (1800–1899)
        - (1900–1999)
        A (2000–2099).
"""
_CENTURY_SIGN = "-"
_CHARS = ["0", "1", "2", "3", "4", "5",
          "6", "7", "8", "9", "A", "B",
          "C", "D", "E", "F", "H", "J",
          "K", "L", "M", "N", "P", "R",
          "S", "T", "U", "V", "W", "X", "Y"] #(ambiguous letters G, I, O, Q, and Z are not used
_DIVIDER = 31

def _get_day_month_year() -> int:
    ymd = datetime.date.today() - relativedelta(years=30)
    return int(ymd.strftime("%d%m%y"))


def _get_three_random_numbers() -> int:
    # TODO:  valid numbers are from 002-889, write validation to return 3 digits
    # TODO: e.g. if randint = 4 then it has to return 004
    return random.randint(101, 899)  # 900–999 are used for temporary personal identification


def _concatenate(*args: str) -> str:
    """
    :param: args numbers which needs to be concatenated
    :return: number as an integer
    """
    result = ''
    for element in args:
        result += str(element)
    return str(result)


def _get_checksum_char(number: int) -> str:
    remainder_split = (str(number / _DIVIDER)).split(".")  # need to have a remainder of division
    remainder_part = float("0." + remainder_split[len(remainder_split) - 1])  # take remainder as a float e.g. 0.123456
    char_position = round(remainder_part * _DIVIDER)
    return str(_CHARS[char_position])

@keyword(name="Get Finnish private SSN")
def get_finnish_ssn():
    ddmmyy = _get_day_month_year()
    ccc = _get_three_random_numbers()
    ddmmyyzccc = _concatenate(ddmmyy, _CENTURY_SIGN, ccc)
    ddmmyyccc = int(_concatenate(ddmmyy, ccc))
    ssn = _concatenate(ddmmyyzccc, _get_checksum_char(ddmmyyccc))
    return ssn

