'''
Created on 09-Sep-2020

@author: maestro
'''

from Sweden.PrivateSSN import generate_swedish_ssn

if __name__ == '__main__':
    print(f"Swedish private ssn number: {generate_swedish_ssn()}")