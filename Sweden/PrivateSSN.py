import datetime
from dateutil.relativedelta import relativedelta
import random

from robot.api.deco import keyword


def _get_year_month_day():
    """
    Function to get date in format: YYMMDD needed to construct Swedish SSN
    
    :return: date in format YYMMDD as an integer
    """
    year_month_day = datetime.date.today() - relativedelta(years=30)
    return int(year_month_day.strftime("%y%m%d"))


def _generate_three_random_numbers():
    """
    :return: 3 digit random number
    """
    return random.randint(101, 999)


def _concatenate_numbers(*args):
    """
    Function needed to concatenate YYMMDD with 3-digit random number
    
    Needed to generate Swedish SSN
    e.g. _concatenate_numbers(12,74) = 1274
    :param: args numbers which needs to be concatenated
    :return: number as an integer
    """
    result = ''
    for element in args:
        result += str(element)
    return int(result)


def _sum_odd_numbers(number):
    """
    Sums numbers on odd position from given argument
    
    :param: number: given number
    :return: sum of odd numbers from given :param
    """
    odd_ssn_numbers = str(number)[::2]
    odd_sum = 0
    for i in odd_ssn_numbers:
        digit_sum = int(i) * 2
        if len(str(digit_sum)) == 2:
            odd_sum += int(str(digit_sum)[0]) + int(str(digit_sum)[1])
        else:
            odd_sum += digit_sum
    return odd_sum


def _sum_even_numbers(number):
    """
    Sums numbers on even posisition from given argument
    
    :param: number: given number
    :return: sum of even numbers from given :param
    """
    even_ssn_numbers = str(number)[1::2]
    even_sum = 0
    for even_pos_number in even_ssn_numbers:
        even_sum += int(even_pos_number)
    return even_sum


def _sum_of_even_odd_numbers(number):
    """
    Function for adding sum of digits on even and odd positions
    
    :param: number:
    :return: overall sum of digits on evene and odd positions
    """
    return _sum_even_numbers(number) + _sum_odd_numbers(number)

@keyword('Get Swedish Private SSN')
def generate_swedish_ssn():
    """
    Genereates Swedish SSN number.
    
    SSN Number is based on Luhn algorithm
    :return: 10-digit swedish ssn number
    """
    ssn_part1 = _concatenate_numbers(_get_year_month_day(), _generate_three_random_numbers())
    sum_odd_even = _sum_of_even_odd_numbers(ssn_part1)
    checksum_modulo_digit = sum_odd_even % 10
    if checksum_modulo_digit == 0:
        checksum_digit = int(checksum_modulo_digit)
    else:
        checksum_digit = 10 - int(str(sum_odd_even)[1])
    return str(ssn_part1) + str(checksum_digit)
