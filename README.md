# README #

Available keywords for Robot Framework (3.1.2+)

### What is this repository for? ###

Repository used to generate fake Nordic (FInnish, Sweden, Norwegian) social security numbers 

### How do I get set up? ###

In a test case just import a library and assign keyword to a variable, e.g.

```python
Library    PrivateSSN.py
...
${finnish_priv_ssn}    Get Finnish private SSN
...
Log   ${finnish_priv_ssn}
```
